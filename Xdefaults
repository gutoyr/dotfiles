! Author: Gustavo Yokoyama Ribeiro
! Update: 20091031 01:10:27
! (C) Copyright 2010 Gustavo Yokoyama Ribeiro
! Licensed under CreativeCommons Attribution-ShareAlike 3.0 Unsupported
! http://creativecommons.org/licenses/by-sa/3.0/ for more info.

!xrdb -merge ~/.gyr.d/dotfiles/Xdefaults
!xrdb -load ~/.gyr.d/dotfiles/Xdefaults
!Query resources
!xrdb -query | grep
!Reload .Xdefaults
!xrdb -override ~/.gyr.d/dotfiles/Xdefaults

#ifndef HOME
#define HOME /home/gyr
#endif

*international: true

# SUSE
#define d_black        #231f20
#define d_red          #ff0000
#define d_green        #63c94a
#define d_yellow       #f78d1f
#define d_blue         #3158af
#define d_magenta      #d6058d
#define d_cyan         #26b7b0
#define d_white        #dedede

#define l_black        #a7a6a6
#define l_red          #ee551c
#define l_green        #b3e763
#define l_yellow       #fbe17e
#define l_blue         #35bee8
#define l_magenta      #f899d1
#define l_cyan         #66cdda
#define l_white        #ffffff

#define s_highlight      #33b87e

*background:   l_white
*foreground:   d_black
*cursorColor:  d_green
*cursorColor2: d_black
*highlightTextColor: l_white
*highlightColor:     s_highlight

*highlightColorMode: true
*highlightReverse:   false

*color0:       d_black
*color1:       d_red
*color2:       d_green
*color3:       d_yellow
*color4:       d_blue
*color5:       d_magenta
*color6:       d_cyan
*color7:       d_white
*color8:       l_black
*color9:       l_red
*color10:      l_green
*color11:      l_yellow
*color12:      l_blue
*color13:      l_magenta
*color14:      l_cyan
*color15:      l_white

!--------------------------------------------------------------------
!*customization: -color
!/etc/X11/rgb.txt

! GYR
!#define S_black         black
!#define S_darkred       red3
!#define S_darkgreen     green4
!#define S_darkyellow    yellow3
!#define S_darkblue      royalblue
!#define S_violet        magenta3
!#define S_turquoise     cyan4
!#define S_gray          lightgray
!
!#define S_darkgray      darkgray
!#define S_red           red
!#define S_green         green1
!#define S_yellow        yellow
!#define S_blue          skyblue1
!#define S_magenta       magenta1
!#define S_cyan          cyan
!#define S_white         white
!
!#define S_orange        orange
!
!*background:   S_black
!*foreground:   S_white
!*cursorColor:  S_green
!*cursorColor2: S_black
!*highlightTextColor: S_black
!*highlightColor:     S_darkgray
!
!*highlightColorMode: true
!*highlightReverse:   false
!
!*color0:       S_black
!*color1:       S_darkred
!*color2:       S_darkgreen
!*color3:       S_darkyellow
!*color4:       S_darkblue
!*color5:       S_violet
!*color6:       S_turquoise
!*color7:       S_gray
!*color8:       S_darkgray
!*color9:       S_red
!*color10:      S_green
!*color11:      S_yellow
!*color12:      S_blue
!*color13:      S_magenta
!*color14:      S_cyan
!*color15:      S_white

!!!*StringConversionWarnings: on

*saveLines: 10000
*scrollBar: false

!!!*backarrowKey: true
!!!*backarrowKeyIsErase: true


! add "!#%+-./~" to charClass (word selection at doubleclick)
*charClass: 33:48,35:48,37:48,43:48,45-47:48,64:48,126:48
!           !     #     %     +     -./      @     ~
! make xterm treats URLs as a single word:
!XTerm*charClass: 33:48,36-47:48,58-59:48,61:48,63-64:48,95:48,126:48

! cssh (clusterssh  ----------------------------------------------------------
CSSH*background: #121212
CSSH*foreground: S_light

! xterm ----------------------------------------------------------------------
xterm*termName:  xterm-256color
*termtype:     vt100

*VT100.Translations:       #override \n \
    Ctrl <Key>F1:         string("\033[1;5P") \n \
    <Key>BackSpace:       string(0x7F) \n \
    Ctrl <KeyPress>Left:  string("\033[90~")  \n \
    Ctrl <KeyPress>Right: string("\033[91~")  \n \
    Ctrl <KeyPress>Up:    string("\033[92~")  \n \
    Ctrl <KeyPress>Down:  string("\033[93~")  \n \
    Shift <Key>Left:      string("\033[1;2D") \n \
    Shift <Key>Right:     string("\033[1;2C") \n \
    Shift <Key>Up:        scroll-back(1,line) \n \
    Shift <Key>Down:      scroll-forw(1,line) \n \
    Shift <Key>Return:    string("\033[24$")  \n \
    Ctrl <Key>Tab:        string(0x1b) string("[rC;TAB~")
!    Ctrl <Key>BackSpace:  string(0x1b) string("[rC;BS~")

!*VT100*translations:    #override \n\
!    Shift~Ctrl<Btn2Up>:  insert-selection(CLIPBOARD, CUT_BUFFER1) \n\
!    Shift<BtnUp>:        select-end(CLIPBOARD, CUT_BUFFER1)

! makes xterm run x-www-browser on the selection when it receives Alt + left click. (Adjust for whatever your Meta key is.)
!*VT100*translations: #override Meta <Btn1Up>: exec-formatted("x-www-browser '%t'", PRIMARY)

! run 'xfontsel'
!xterm*faceName:           terminus:medium:pixelsize=12
!*font: -xos4-terminus-*-*-*-*-*-*-*-*-*-*-*-*
xterm*faceName:           inconsolata:medium:pixelsize=12
!*font: -*-terminus-*-*-*-*-*-*-*-*-*-*-*-*
!*font: -*-proggycleansz-*-*-*-*-*-*-*-*-*-*-*-*
!xterm*faceName: ProggyCleanTTSZ
!xterm*faceSize: 12
!*font: xft:ProggyCleanTTSZ:pixelsize=12
!*font: -windows-proggycleansz-medium-r-normal--13-80-96-96-c-70-iso8859-1
!!*boldFont: -windows-proggycleansz-medium-r-normal--13-80-96-96-c-70-iso8859-1
!xterm*font: -windows-proggycleansz-medium-r-normal--13-80-96-96-c-70-iso8859-1
!xterm*boldFont: -windows-proggycleansz-medium-r-normal--13-80-96-96-c-70-iso8859-1
!!*wideChars: false
!xterm*boldMode:  False
!xterm*alwaysBoldMode: False
!xterm*boldColors: False
!xterm*colorBDMode: true
!xterm*colorBD: white
!xterm*dynamicColors: true

XTerm*bellIsUrgent: true
*borderWidth: 0
*metaSendsEscape: true

! Xft ------------------------------------------------------------------------
Xft.dpi:       96
Xft.antialias: true
Xft.hinting: true
Xft.hintstyle: hintlight
!Xft.hintstyle: hintfull
Xft.lcdfilter: lcddefault
Xft.rgba: rgb

! rxvt-unicode ---------------------------------------------------------------
urxvt*VT100.Translations:       #override \n \
    Shift <Key>Space:     string("\033[23$")

! man 7 urxvt
! blueish
!URxvt.colorBD:       S_dark
!URxvt.colorUL:       S_dark
!URxvt.colorIT:       S_dark
! whitred
URxvt.colorBD:       red
URxvt.colorUL:       red
URxvt.colorIT:       red
!urxvt*font:          terminus-12
!URxvt*termName: rxvt-unicode
!URxvt*termName: xterm-256color
!URxvt.font:-*-terminus-*-*-*-*-12-*-*-*-*-*-*-*
URxvt.depth: 32
!URxvt.font: xft:Terminus-9
!!URxvt.font: xft:Terminus:size=9
!!URxvt.boldFont: xft:Terminus:bold:size=9
!!URxvt.italicFont: xft:Terminus:italic:size=9
!!URxvt.boldItalicFont: xft:Terminus:bold:italic:size=9

! http://git.ak-online.be/kandre/xsession/tree

URxvt*font: terminus-12,xft:Terminus for Powerline:style=Medium,xft:Code2000:antialias=false
URxvt*boldFont: terminus-bold-12,xft:Terminus for Powerline:style=Bold,xft:Code2000:style=Bold:antialias=false
!URxvt*font: terminus-12,xft:Terminus for Powerline:antialias=false
!URxvt*boldFont: terminus-bold-12,xft:Terminus for Powerline:style=Bold:antialias=false
!URxvt*italicFont: -xos4-terminus-medium-o-normal--12-120-72-72-c-60-iso10646-1
!URxvt*boldItalicFont: -xos4-terminus-bold-o-normal--0-0-72-72-c-0-iso10646-1
!URxvt.letterSpace: -1
URxvt.iso14755: False
URxvt*buffered: true
!URxvt*font:     xft:terminus:pixelsize=12:antialias=false, \
!                xft:inconsolata:bold:pixelsize=14
!URxvt*boldFont:     xft:inconsolata:bold:pixelsize=14
!URxvt*italicFont:     xft:inconsolata:italic:pixelsize=14
!URxvt*boldItalicFont:     xft:inconsolata:bold:italic:pixelsize=14

! -- also: set 'vbell on' in screen
! -- set 'bell_msg' "\a in window!"
URxvt*urgentOnBell:          True
URxvt.mapAlert:              True
URxvt*tripleclickwords:      True
URxvt.loginShell:            False
URxvt.pastableTabs:          False
URxvt.utmpInhibit:           True
URxvt.scrollstyle:           plain
URxvt.scrollTtyOutput:       False
URxvt.scrollWithBuffer:      True
URxvt.jumpScroll:            True
URxvt.scrollTtyKeypress:     True
URxvt.mouseWheelScrollPage:  True
URxvt*skipBuiltinGlyphs:     True
URxvt.cursorBlink:           True
URxvt.intensityStyles:      False

! borderless and no scrollbar
URxvt*scrollBar_right: false
URxvt*scrollBar: false
URxvt*borderLess: false
! the transparency stuff
!!URxvt*inheritPixmap: true
!URxvt*transparent: True
!blueish
!URxvt*tintColor: white
! whitred
URxvt*tintColor: black
!URxvt*shading: 20
!URxvt*fading: 30
!!URxvt*cursorUnderline: True
! Extensions
urxvt*perl-lib:        /usr/lib/urxvt/perl/
urxvt*perl-ext-common: default,matcher,searchable-scrollback,bell-command,selection-autotransform
urxvt*urlLauncher:     /usr/bin/xdg-open
urxvt*matcher.button:  3
urxvt*bell-command: notify-send "Beep, Beep"
urxvt*keysym.C-Delete:  perl:matcher:last
urxvt*keysym.M-Delete:  perl:matcher:list
! bluish
!URxvt.tabbed.tabbar-fg: 0
!URxvt.tabbed.tabbar-bg: 7
!URxvt.tabbed.tab-fg:    7
!URxvt.tabbed.tab-bg:    0
! whitred
URxvt.tabbed.tabbar-fg: 0
URxvt.tabbed.tabbar-bg: 9
URxvt.tabbed.tab-fg:    9
URxvt.tabbed.tab-bg:    0
URxvt.selection-autotransform.0: s/^([^:[:space:]]+):(\\d+):?$/vim +$2 \\Q$1\\E\\x0d/

URxvt.keysym.M-C-1: command:\033]50;-*-terminus-*-*-*-*-12-*-*-*-*-*-*-*\007
URxvt.keysym.M-C-2: command:\033]50;-*-terminus-*-*-*-*-14-*-*-*-*-*-*-*\007
URxvt.keysym.M-C-3: command:\033]50;-*-terminus-*-*-*-*-18-*-*-*-*-*-*-*\007
URxvt.keysym.C-Up: command:\033]720;1\007
URxvt.keysym.C-Down: command:\033]721;1\007
!URxvt.keysym.Shift-Up: command:\033]720;1\007
!URxvt.keysym.Shift-Down: command:\033]721;1\007
!URxvt.keysym.KP_Up: ^[[A
!URxvt.keysym.KP_Right: ^[[C
!URxvt.keysym.KP_Left: ^[[D
!URxvt.keysym.KP_Down: ^[[B
!URxvt.keysym.KP_Insert: ^[[2~
!URxvt.keysym.KP_Delete: ^[[3~
!URxvt.keysym.KP_Add: +
!URxvt.keysym.KP_Subtract: -
!URxvt.keysym.KP_Multiply: *
!URxvt.keysym.KP_Divide: /
!URxvt.keysym.KP_Enter: ^M
!! map shift-space with Shift-F11, for Vim.
!! http://vim.wikia.com/wiki/Avoid_the_escape_key
URxvt.keysym.S-space: \033[23$
URxvt.keysym.S-Return: \033[24$
URxvt.keysym.Control-Tab:        \033[rC;TAB~
!URxvt.keysym.Control-BackSpace:  \033[rC;BS~

! icon path
URxvt.iconFile:     /usr/share/pixmaps/urxvt.xpm

! x11-ssh-askpass ------------------------------------------------------------
x11-ssh-askpass*grabServer: false
x11-ssh-askpass*grabPointer: false
x11-ssh-askpass*grabKeyboard: false
ssh-askpass*grabServer: false
ssh-askpass*grabPointer: false
ssh-askpass*grabKeyboard: false
*grabServer: false
*grabPointer: false
*grabKeyboard: false

! Highlight mark ends at the last character of each line
*highlightSelection: true

! yeahconsole ----------------------------------------------------------------
!yeahconsole*screenWidth:   1200
!!yeahconsole*xOffset:       120
!!!yeahconsole*consoleHeight: 40
!!!yeahconsole*toggleKey:     None+F1
!!!yeahconsole*keyFull:       None+F2
!!!yeahconsole*font: -windows-proggycleansz-medium-r-normal--13-80-96-96-c-70-iso8859-1
!!!yeahconsole*boldFont: -windows-proggycleansz-medium-r-normal--13-80-96-96-c-70-iso8859-1
!yeahconsole*restart: 1
!yeahconsole*stepSize: 0
irssi*urgentOnBell:          True
weechat*urgentOnBell:          True
Weechat.iconFile:   /usr/share/pixmaps/weechat.xpm
Mutt.iconFile:      /usr/share/pixmaps/mutt.xpm
